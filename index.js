/**
 * Created by mario on 12/03/17.
 */
const socketIOClient = require('socket.io-client'),
    sailsIOClient = require('sails.io.js'),
    Promise = require('bluebird'),
    _ = require('lodash');

const io = sailsIOClient(socketIOClient);

const SailsPromise = {

    config(c) {
        _.merge(io.sails, c);
    },

    get(path, params) {
        params = params || {};
        const p = new Promise(function (resolve, reject) {
            io.socket.get(path, params, (body, jwr) => {
                if (jwr.statusCode < 200 || jwr.statusCode >= 400) return reject(jwr.error);
                return resolve({data: body});
            });
        });
        return p;
    },

    post(path, params) {
        params = params || {};
        const p = new Promise(function (resolve, reject) {
            io.socket.post(path, params, (body, jwr) => {
                if (jwr.statusCode < 200 || jwr.statusCode >= 400) return reject(jwr.error);
                return resolve({data: body});
            });
        });
        return p;
    },

    put(path, params) {
        params = params || {};
        const p = new Promise(function (resolve, reject) {
            io.socket.put(path, params, (body, jwr) => {
                if (jwr.statusCode < 200 || jwr.statusCode >= 400) return reject(jwr.error);
                return resolve({data: body});
            });
        });
        return p;
    },

    delete(path, params) {
        params = params || {};
        const p = new Promise(function (resolve, reject) {
            io.socket.delete(path, params, (body, jwr) => {
                if (jwr.statusCode < 200 || jwr.statusCode >= 400) return reject(jwr.error);
                return resolve({data: body});
            });
        });
        return p;
    },

    on(event, cb) {
        return io.socket.on(event, (msg) => {
            return cb(msg);
        });
    },

    off(event) {
        return io.socket.off(event);
    },

    request(options) {
        const p = new Promise(function (resolve, reject) {
            io.socket.delete(path, (body, jwr) => {
                if (jwr.statusCode < 200 || jwr.statusCode >= 400) return reject(jwr.error);
                return resolve({data: body});
            });
        });
        return p;
    },

    initializeApp(config) {
        _.merge(io.sails, c);
    },

    auth() {
        const _self = this;
        return {
            signInWithIdentifierAndPassword(identifier, password) {
                return _self.post('/auth/local',{identifier,password});
            },
            createUserWithIdentifierAndPassword(identifier, password) {
                return _self.post('/user',{identifier,password});
            },
            isAuthenticated() {
                return _self.get('/user/me');
            }
        }
    }

};

module.exports = SailsPromise;
