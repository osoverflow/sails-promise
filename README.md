# sails-promise #

Sails socket communications using promise with bluebird.

### Motivation ###

* Using sails-socket with mobile apps with react
* Socket is a lot faster than simple post/get requests
* Use promisified socket requests to make it easier to manage data from sails
* Use also a custom authentication mechanism just like firebird and other services but with sails

### How do I get set up? ###

* Install
Install with `npm --save install sails-promise`

* Configuration

```javascript
SailsPromise.config({config object})
```

Only required parameter is url (for react-native)

* Dependencies
+ [socket.io-client](https://www.npmjs.com/package/socket.io-client)
+ [sails.io.js](https://www.npmjs.com/package/sails.io.js)
+ [bluebird](https://www.npmjs.com/package/bluebird)
+ [lodash](https://www.npmjs.com/package/lodash)

* Deployment instructions
It is tested on react and react-native applications with Redux.

Only install and import, and configure it.

Example:

```javascript
import SailsPromise from 'sails-promise';
SailsPromise.config({url: 'http://localhost:1337'});


export function action_fetchSomething() {
  const request = SailsPromise.get('/something',{});
  return {
    type: 'fetch_something',
    payload: request
  };
}
```

### Who do I talk to? ###

* Repo owner or admin

